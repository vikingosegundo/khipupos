//
//  AppState.swift
//  ELPModels
//
//  Created by Manuel Meyer on 11.08.22.
//

struct AppState: Codable {
    enum Change {
        case add(Add); enum Add{
            case order(Order)
        }
        case remove(Remove); enum Remove {
            case order(Order)
        }
    }
    public func alter(by changes:[Change]  ) -> Self { changes.reduce(self) { $0.alter(by:$1) } }
    public func alter(by changes: Change...) -> Self { changes.reduce(self) { $0.alter(by:$1) } }

    let orders:[Order]
    init() {
        orders = []
    }
}
private extension AppState {
    init(_ o:[Order]) { orders = o }
    func alter(by change:Change) -> Self {
        switch change {
        case let .add(.order(o)): return Self(orders + [o])
        case let .remove(.order(o)): return Self(orders.filter{ $0.id != o.id })
        }
    }
}
