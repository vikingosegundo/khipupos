//
//  Model.swift
//  KhipuPOS
//
//  Created by vikingosegundo on 27/01/2023.
//

import Foundation

struct Table: Codable {
    let tableID:String
}
struct Article: Codable {
    let name:String
    let price:Decimal
}
struct Order:Identifiable, Codable {
    enum Change {
        case add(Add)           ; enum Add      {
            case position(Position)
            case funds(Decimal)
        }
        case remove(Remove)     ; enum Remove   { case position(Position) }
        case increase(Increase) ; enum Increase { case count(by:UInt,for:Position) }
        case decrease(Increase) ; enum Decrease { case count(by:UInt,for:Position) }
        case change(to:Table)
    }
    let id: UUID
    let positions:[Position]
    let table:Table
    let funds:Decimal
    init(table t:Table) { self.init(UUID(),t,[], 0) }
    func alter(_ c:Change...) -> Self {c.reduce(self) { $0.alter($1) }}
    var totalPrice:Decimal { positions.reduce(0) { $0 + $1.price } - funds }
}

extension Order {
    private init(_ i:UUID,_ t:Table,_ p:[Position],_ f:Decimal) { id = i; table = t; positions = p; funds = f }
    private func alter(_ c:Change) -> Self {
        switch c {
        case let .add     (.position(p)      ): return p.count > 0 ? .init(id,table,sort(positions + [p]), funds) : self
        case let .remove  (.position(p)      ): return               .init(id,table,sort( positions.filter {!($0.count == p.count && $0.article == p.article) }                                   ), funds)
        case let .change  (to: t             ): return               .init(id,t,    sort( positions                                                                                               ), funds)
        case let .increase(.count(by:i,for:p)): return               .init(id,table,sort([positions.first(where: { $0.id == p.id})!.alter(.inrease(.by(i)))] + positions.filter({ $0.id != p.id })), funds)
        case let .decrease(.count(by:d,for:p)): return               .init(id,table,sort([positions.first(where: { $0.id == p.id})!.alter(.derease(.by(d)))] + positions.filter({ $0.id != p.id })), funds)
        case let .add(.funds(f)): return .init(id,table, positions, funds + f)
        }
    }
}

struct Position:Identifiable, Codable {
    enum Change {
        case inrease(Increase); enum Increase { case by(UInt) }
        case derease(Increase); enum Decrease { case by(UInt) }
    }
    init(count c: Int, article a:Article) { self.init(UUID(),c, a, .now) }
    private init(_ i:UUID,_ c: Int,_ a:Article,_ cr:Date) { id = i; count = c; article = a; created = cr }
    
    func alter(_ c:Change) -> Self {
        switch c {
        case .inrease(.by(let i)): return                .init(id,count+Int(i),article,created)
        case .derease(.by(let i)): return (i <= count) ? .init(id,count-Int(i),article,created) : self
        }
    }
    let id     : UUID
    let created: Date
    let count  : Int
    let article: Article
    var price  : Decimal { Decimal(count) * article.price }
}

func sort(_ pos:[Position]) -> [Position] { pos.sorted(by: {$0.created < $1.created}) }

extension Table   :CustomStringConvertible { var description: String { "table: \(tableID)"} }
extension Article :CustomStringConvertible { var description: String { "\(name) (\(price))" } }
extension Position:CustomStringConvertible { var description: String { "\(count) \(article)" } }
extension Order   :CustomStringConvertible { var description: String { "\(table)\(positions.reduce("", {$0 + "\n\($1)"})) \ntotal: \(totalPrice)"} }
extension Table   :Equatable                {}
extension Article :Equatable                {}
extension Position:Equatable                {}
extension Order   :Equatable                {}
