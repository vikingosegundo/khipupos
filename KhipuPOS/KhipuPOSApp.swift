//
//  KhipuPOSApp.swift
//  KhipuPOS
//
//  Created by vikingosegundo on 27/01/2023.
//

import SwiftUI

@main
struct KhipuPOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
