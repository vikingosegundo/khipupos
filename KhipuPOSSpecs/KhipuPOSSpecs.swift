//
//  KhipuPOSSpecs.swift
//  KhipuPOSSpecs
//
//  Created by vikingosegundo on 27/01/2023.
//

import Quick
import Nimble
@testable import KhipuPOS

final class AppStateSpecs: QuickSpec {
    override func spec() {
        describe("AppState") {
            let appState = AppState()
            context("creation") {
                it("with no orders") { expect(appState.orders).to(haveCount(0))}
            }
            context("adding order") {
                let appState0 = appState.alter(by: .add(.order(Order(table:Table(tableID: "1")))))
                it("with one orders") { expect(appState0.orders).to(haveCount(1))}
                context("removing order") {
                    let o = appState0.orders.first!
                    let appState1 = appState0.alter(by: .remove(.order(o)))
                    it("it has no orders") { expect(appState1.orders).to(haveCount(0))}
                }
            }
            context("Store") {
                var store:Store<AppState,AppState.Change>!
                var wasUpdated:Bool!
                beforeEach {
                    wasUpdated = false
                    store = createDiskStore()
                    store.updated {
                        wasUpdated = true
                    }
                    store.change(.add(.order(.init(table: .init(tableID: "001")))))
                }
                
                it("will inform listeners") { expect(wasUpdated          ).to(beTrue()) }
                it("will have 1 order")     { expect(store.state().orders).to(haveCount(1)) }
                context("reset") {
                    beforeEach {
                        store.reset()
                    }
                    it("will have no order"   ) { expect(store.state().orders).to(beEmpty()) }
                    it("will inform listeners") { expect(wasUpdated          ).to(beTrue() ) }
                }
                context("destroy") {
                    beforeEach {
                        destroy(&store)
                    }
                    it("will be nil") { expect(store).to(beNil()) }
                }
            }
        }
    }
}

final class OrderSpecs: QuickSpec {
    override func spec() {
        describe("Order") {
            // Prepartion
            let t = Table(tableID: "001")
            let t1 = Table(tableID: "002")

            let ps = [ // Positions
                (1, "Milky Rice", 4.5),
                (2, "Coffee"    , 2.5)
            ].reduce([]) {$0 + [
                Position(count:$1.0,article:Article(name:$1.1,price:$1.2))
            ]}
            // Begin Tests
            let o0 = Order(table: t)
            context("creation") {
                it("has no positions"           ) { expect(o0.positions  ).to(beEmpty()                     ) }
                it("has total price 0"          ) { expect(o0.totalPrice ).to(equal(0)                      ) }
                it("has funds of 0.0"           ) { expect(o0.funds      ).to(equal(0.0)                    ) }
                it("has a string representation") { expect(o0.description).to(equal("table: 001 \ntotal: 0")) }
            }
            context("adding positions") {
                let o1 = o0.alter(
                    .add(.position(ps[0])),
                    .add(.position(ps[1]))
                )
                it("has changed positions"      ) { expect(o1.positions           ).toNot(equal(o0.positions)                                              ) }
                it("has 2 positions"            ) { expect(o1.positions           ).to   (haveCount(2)                                                     ) }
                it("has an expected order"      ) { expect(o1.positions[0].article).to   (equal(ps[0].article)                                             ) }
                it("has an expected order"      ) { expect(o1.positions[1].article).to   (equal(ps[1].article)                                             ) }
                it("has a changed total"        ) { expect(o1.totalPrice          ).toNot(equal(o0.totalPrice)                                             ) }
                it("has a total price of 9.5"   ) { expect(o1.totalPrice          ).to   (equal(9.5)                                                       ) }
                it("has funds of 0.0"           ) { expect(o1.funds               ).to   (equal(0.0)                                                       ) }
                it("has a string representation") { expect(o1.description         ).to(equal("table: 001\n1 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: 9.5")) }
                context("increase amount Milky Rice") {
                    let o2 = o1.alter(.increase(.count(by:2,for:o1.positions.first!)))
                    it("has same number of positions"          ) { expect(o2.positions           ).to(haveCount(o1.positions.count)                                        ) }
                    it("has amount of first position increased") { expect(o2.positions[0].count  ).to(equal(o1.positions[0].count + 2)                                     ) }
                    it("has a total price of 18.5"             ) { expect(o2.totalPrice          ).to(equal(18.5)                                                          ) }
                    it("has funds of 0.0"                      ) { expect(o2.funds               ).to(equal(0.0)                                                           ) }
                    it("has an expected order"                 ) { expect(o2.positions[0].article).to(equal(ps[0].article)                                                 ) }
                    it("has an expected order"                 ) { expect(o2.positions[1].article).to(equal(ps[1].article)                                                 ) }
                    it("has a string representation") { expect(o2.description                    ).to(equal("table: 001\n3 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: 18.5")) }
                    context("decrease"){
                        let o3 = o2.alter(.decrease(.count(by:1,for:o2.positions.first!)))
                        it("has same number of positions"          ) { expect(o3.positions             ).to(haveCount(o2.positions.count)                                      ) }
                        it("has amount of first position increased") { expect(o3.positions.first!.count).to(equal(o2.positions.first!.count - 1)                               ) }
                        it("has a total price of 14.0"             ) { expect(o3.totalPrice            ).to(equal(14.0)                                                        ) }
                        it("has funds of 0.0"                      ) { expect(o3.funds                 ).to(equal(0.0)                                                         ) }
                        it("has a string representation"           ) { expect(o3.description           ).to(equal("table: 001\n2 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: 14")) }
                        context("decrease too much fails silently"){
                            let o4 = o3.alter(.decrease(.count(by:4,for:o2.positions.first!)))
                            it("has same number of positions"          ) { expect(o4.positions             ).to(haveCount(o3.positions.count)                                      ) }
                            it("has amount of first position increased") { expect(o4.positions.first!.count).to(equal(o3.positions.first!.count)                                   ) }
                            it("has a total price of 14.0"             ) { expect(o4.totalPrice            ).to(equal(14.0)                                                        ) }
                            it("has funds of 0.0"                      ) { expect(o4.funds                 ).to(equal(0.0)                                                         ) }
                            it("has a string representation"           ) { expect(o4.description           ).to(equal("table: 001\n2 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: 14")) }
                        }
                    }
                }
                context("removing position") {
                    let o2 = o1.alter(.remove(.position(o1.positions.first!)))
                    it("has one position"                 ) { expect(o2.positions  ).to   (haveCount(1)                                  ) }
                    it("has different number of positions") { expect(o2.positions  ).toNot(haveCount(o1.positions.count)                 ) }
                    it("has a total price of 5.0"         ) { expect(o2.totalPrice ).to   (equal(5.0)                                    ) }
                    it("has funds of 0.0"                 ) { expect(o2.funds      ).to   (equal(0.0)                                    ) }
                    it("has a string representation"      ) { expect(o2.description).to   (equal("table: 001\n2 Coffee (2.5) \ntotal: 5")) }
                }
                
                context("switching tables") {
                    let o1 = o0.alter(
                        .add(.position(ps.first!)),
                        .add(.position(ps.last!))
                    ).alter(.change(to: t1))
                    it("is assigned to table 2"     ) { expect(o1.table.tableID).to(equal("002"))}
                    it("has changed positions"      ) { expect(o1.positions               ).toNot(equal(o0.positions)                                              ) }
                    it("has 2 positions"            ) { expect(o1.positions               ).to   (haveCount(2)                                                     ) }
                    it("has an expected order"      ) { expect(o1.positions.first!.article).to   (equal(ps.first!.article)                                         ) }
                    it("has an expected order"      ) { expect(o1.positions.last! .article).to   (equal(ps.last! .article)                                         ) }
                    it("has a changed total"        ) { expect(o1.totalPrice              ).toNot(equal(o0.totalPrice)                                             ) }
                    it("has a total price of 9.5"   ) { expect(o1.totalPrice              ).to   (equal(9.5)                                                       ) }
                    it("has funds of 0.0"           ) { expect(o1.funds                   ).to   (equal(0.0)                                                       ) }
                    it("has a string representation") { expect(o1.description             ).to(equal("table: 002\n1 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: 9.5")) }
                }
                context("paying") {
                    let o1 = o0.alter(
                        .add(.position(ps.first!)),
                        .add(.position(ps.last!))
                    ).alter(.change(to: t1))
                    it("is assigned to table 2"     ) { expect(o1.table.tableID           ).to   (equal("002"))}
                    it("has changed positions"      ) { expect(o1.positions               ).toNot(equal(o0.positions)                                                 ) }
                    it("has 2 positions"            ) { expect(o1.positions               ).to   (haveCount(2)                                                        ) }
                    it("has an expected order"      ) { expect(o1.positions.first!.article).to   (equal(ps.first!.article)                                            ) }
                    it("has an expected order"      ) { expect(o1.positions.last! .article).to   (equal(ps.last! .article)                                            ) }
                    it("has a changed total"        ) { expect(o1.totalPrice              ).toNot(equal(o0.totalPrice)                                                ) }
                    it("has a total price of 9.5"   ) { expect(o1.totalPrice              ).to   (equal(9.5)                                                          ) }
                    it("has funds of 0.0"           ) { expect(o1.funds                   ).to   (equal(0.0)                                                          ) }
                    it("has a string representation") { expect(o1.description             ).to   (equal("table: 002\n1 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: 9.5")) }
                    context("adding funds") {
                        let o2 = o1.alter(.add(.funds(9.0)))
                        it("has a changed total"        ) { expect(o2.totalPrice              ).toNot(equal(o1.totalPrice)                                           ) }
                        it("has a total price of 9.5"   ) { expect(o2.totalPrice              ).to   (equal(0.5)                                                     ) }
                        it("has funds of 9.0"           ) { expect(o2.funds                   ).to   (equal(9.0)                                                     ) }
                        it("is assigned to table 2"     ) { expect(o2.table.tableID           ).to   (equal("002")                                                   ) }
                        it("has unchanged positions"    ) { expect(o2.positions               ).to   (equal(o1.positions)                                            ) }
                        it("has 2 positions"            ) { expect(o2.positions               ).to   (haveCount(2)                                                   ) }
                        it("has an expected order"      ) { expect(o2.positions.first!.article).to   (equal(ps.first!.article)                                       ) }
                        it("has an expected order"      ) { expect(o2.positions.last! .article).to   (equal(ps.last! .article)                                       ) }
                        it("has a string representation") { expect(o2.description             ).to   (equal("table: 002\n1 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: 0.5")) }
                        context("adding missing funds") {
                            let o3 = o2.alter(.add(.funds(0.5)))
                            it("has a changed total"        ) { expect(o3.totalPrice              ).toNot(equal(o2.totalPrice)                                                ) }
                            it("has a total of 0.0"         ) { expect(o3.totalPrice              ).to   (equal(0)                                                            ) }
                            it("has funds of 9.0"           ) { expect(o3.funds                   ).to   (equal(9.5)                                                          ) }
                            it("is assigned to table 2"     ) { expect(o3.table.tableID           ).to   (equal("002")                                                        ) }
                            it("has unchanged positions"    ) { expect(o3.positions               ).to   (equal(o2.positions)                                                 ) }
                            it("has 2 positions"            ) { expect(o3.positions               ).to   (haveCount(2)                                                        ) }
                            it("has an expected order"      ) { expect(o3.positions.first!.article).to   (equal(ps.first!.article)                                            ) }
                            it("has an expected order"      ) { expect(o3.positions.last! .article).to   (equal(ps.last! .article)                                            ) }
                            it("has a string representation") { expect(o3.description             ).to   (equal("table: 002\n1 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: 0")) }
                        }
                        context("adding more funds than needed") {
                            let o3 = o2.alter(.add(.funds(1.0)))
                            it("has a changed total"        ) { expect(o3.totalPrice              ).toNot(equal(o2.totalPrice)                                                 ) }
                            it("has a total of 0.0"         ) { expect(o3.totalPrice              ).to   (equal(-0.5)                                                          ) }
                            it("has funds of 9.0"           ) { expect(o3.funds                   ).to   (equal(10.0)                                                          ) }
                            it("is assigned to table 2"     ) { expect(o3.table.tableID           ).to   (equal("002")                                                         ) }
                            it("has unchanged positions"    ) { expect(o3.positions               ).to   (equal(o2.positions)                                                  ) }
                            it("has 2 positions"            ) { expect(o3.positions               ).to   (haveCount(2)                                                         ) }
                            it("has an expected order"      ) { expect(o3.positions.first!.article).to   (equal(ps.first!.article)                                             ) }
                            it("has an expected order"      ) { expect(o3.positions.last! .article).to   (equal(ps.last! .article)                                             ) }
                            it("has a string representation") { expect(o3.description             ).to   (equal("table: 002\n1 Milky Rice (4.5)\n2 Coffee (2.5) \ntotal: -0.5")) }
                            context("0.5 are to be returned to customer") {
                                it("has a total of -0.5"    ) { expect(o3.totalPrice              ).to   (equal(-0.5)                                                          ) }
                            }
                        }
                    }
                }
            }
        }
    }
}

